from django.shortcuts import render


def home(request):
    return render(request, 'main/home.html')

def mystory(request):
	return render(request, 'main/mystory.html')