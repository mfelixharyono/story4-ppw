from django.test import TestCase, Client
from django.urls import resolve
from .views import books,buku

class story8Test(TestCase):

    def test_Story8_books_ada(self):
        response = Client().get('/Story8/')
        self.assertEqual(response.status_code,200)

    def test_html(self):
    	response = Client().get('/Story8/')
    	html_kembalian = response.content.decode('utf8')
    	self.assertIn('Pencarian Buku', html_kembalian)
    	self.assertTemplateUsed(response, 'books.html')