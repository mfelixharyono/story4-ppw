from django.shortcuts import render,redirect
from django.http import JsonResponse
import urllib
import requests
import json
# Create your views here.

def books(request):
	return render(request,'books.html')

def buku(request):
	tmp = "https://www.googleapis.com/books/v1/volumes?q="+request.GET['q']
	bentar = requests.get(tmp)
	data = json.loads(bentar.content)
	return JsonResponse(data,safe=False)