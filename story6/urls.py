from django.urls import path

from . import views

app_name = 'story6'

urlpatterns = [
    path('', views.daftarkegiatan, name='daftarkegiatan'),
    path('anggota/<int:qid>/',views.tambahanggota, name='tambahanggota'),
    path('kegiatan/',views.buatkegiatan, name='buatkegiatan'),   
]
