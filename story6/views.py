from django.shortcuts import render,redirect

from .forms import kegiatanform, anggotaform

from .models import kegiatan, anggota
# Create your views here.

def buatkegiatan(request):
	if request.method == "POST":
		form = kegiatanform(request.POST)
		if form.is_valid():
			simpan = form.save()	
			simpan.save()
	else:
		form = kegiatanform()

	context = {
		'form' :form,
	}
	return render(request,'buatkegiatan.html',context)

def tambahanggota(request,qid):
	if request.method == "POST":
		form = anggotaform(request.POST)
		if form.is_valid():
			simpan = form.save(commit=False)
			simpan.kegiatan = kegiatan.objects.get(id = qid) 
			simpan.save()
	else:
		form = anggotaform()

	context = {
		'form' :form,
	}
	return render(request,'tambahanggota.html',context)

def daftarkegiatan(request):
	kegiatankeluar = kegiatan.objects.all();
	anggotakeluar  = anggota.objects.all();
	context = {
		'anggotakeluar'	 : anggotakeluar,
		'kegiatankeluar' : kegiatankeluar,
	}
	return render(request,'daftarkegiatan.html',context)