from django import forms

from .models import kegiatan, anggota

class kegiatanform(forms.ModelForm):
	class Meta:
		model = kegiatan

		fields =[
			'Judul',
			'Deskripsi',
		]

		widgets = {
			'Judul'    	 : forms.TextInput(attrs={'placeholder': 'Tulis Judul Kegiatan','class':'form-control'}),
			'Deskripsi'  : forms.TextInput(attrs={'placeholder': 'Tulis Deskripsi','class':'form-control'}),
		}

class anggotaform(forms.ModelForm):
	class Meta:
		model = anggota
		exclude = ('kegiatan',)
		fields =[
			'NamaAnggota',
		]

		help_texts = {
			'NamaAnggota' : None,
		}

		labels = {
			'NamaAnggota' : 'Nama Anggota',
		}

		widgets = {
			'NamaAnggota' : forms.TextInput(attrs={'placeholder': 'Tulis Nama Anda','class':'form-control'}),
		}