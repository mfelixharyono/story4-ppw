from django.test import TestCase, Client
from django.urls import resolve
from .models import kegiatan, anggota
from .views import tambahanggota, buatkegiatan, daftarkegiatan
from .forms import kegiatanform, anggotaform

class story6Test(TestCase):

    def test_story6_daftarkegiatan_ada(self):
        response = Client().get('/story6/')
        self.assertEqual(response.status_code,200)

    def test_story6_buatkegiatan_ada(self):
        response = Client().get('/story6/kegiatan/')
        self.assertEqual(response.status_code,200)

    def test_story6__tambahanggota_ada(self):
   	    kegiatan.objects.create(Judul="Ini Judul", Deskripsi="Ini Deskripsi")
   	    response = Client().get('/story6/anggota/1/')
   	    self.assertEquals(response.status_code,200)

    def test_story6__tambahanggota_POST_ada(self):
    	tmp = kegiatan.objects.create(Judul="Ini Judul", Deskripsi="Ini Deskripsi")
    	c = Client()
    	response = c.post('/story6/anggota/1/', {'NamaAnggota' : "namaanggota"})
    	self.assertEquals(response.status_code,200)

    def test_main_daftarkegiatan_func(self):
        found = resolve('/story6/')
        self.assertEqual(found.func,daftarkegiatan)

    def test_main_kegiatan_func(self):
        found = resolve('/story6/kegiatan/')
        self.assertEqual(found.func,buatkegiatan)

    def test_story6__kegiatan_POST_ada(self):
    	c = Client()
    	response = c.post('/story6/kegiatan/', {'Judul' : "Ini judul",'Deskripsi' : "Ini Deskripsi"})
    	self.assertEquals(response.status_code,200)

    def test_main_mystory_apakah_di_halaman_hasil_ada_templatenya(self):
        response = Client().get('/story6/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn('Daftar Kegiatan', html_kembalian)
        self.assertTemplateUsed(response, 'daftarkegiatan.html')

    def test_main_home_apakah_di_halaman_hasil_ada_templatenya(self):
        response = Client().get('/story6/kegiatan/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn('Judul', html_kembalian)
        self.assertTemplateUsed(response, 'buatkegiatan.html')

    def test_model_kegiatan(self):
    	kegiatan.objects.create(Judul="Ini Judul", Deskripsi="Ini Deskripsi")
    	jumlah_kegiatan = kegiatan.objects.all().count()
    	self.assertEquals(jumlah_kegiatan, 1)

    def test_model_anggota(self):
    	tmp = kegiatan.objects.create(Judul="Ini Judul", Deskripsi="Ini Deskripsi")
    	anggota.objects.create(NamaAnggota ="namaanggota", kegiatan = tmp)
    	jumlah_anggota = anggota.objects.all().count()
    	self.assertEquals(jumlah_anggota, 1)

    def test_form_kegiatanform(self):
    	form = kegiatanform({
    		'Judul': "Ini Judul",
    		'Deskripsi': "Ini Deskripsi",
    	})
    	self.assertTrue(form.is_valid())
    	comment = form.save()
    	self.assertEqual(comment.Judul, "Ini Judul")
    	self.assertEqual(comment.Deskripsi, "Ini Deskripsi")