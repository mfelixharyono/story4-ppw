from django import forms

from .models import matkul

class matkulform(forms.ModelForm):
	class Meta:
		model = matkul

		fields =[
			'MataKuliah',
			'Dosen',
			'SKS',
			'Semester',
		]
		help_texts = {
			'MataKuliah' : None,
		}

		labels = {
			'MataKuliah' : 'Mata Kuliah',
			'Dosen'		 :'Nama Dosen'
		}

		widgets = {
			'MataKuliah' : forms.TextInput(attrs={'placeholder': 'Tulis Nama Mata Kuliah','class':'form-control'}),
			'Dosen' : forms.TextInput(attrs={'placeholder': 'Tulis Nama Dosen','class':'form-control'}),
			'SKS' : forms.TextInput( attrs={'placeholder': 'Tulis Jumlah SKS (Angka)','class':'form-control'}),
			'Semester' : forms.TextInput(attrs={'placeholder': 'Contoh: Gasal 2019/2020','class':'form-control'}),
		}