from django.urls import path

from . import views

app_name = 'story5'

urlpatterns = [
    path('', views.productbuat, name='productbuat'),
    path('detail/<int:qid>/',views.detail, name='detail'),
    path('hapus/<int:qid>/',views.hapus, name='hapus'),   
]
