from django.shortcuts import render,redirect

from .forms import matkulform

from .models import matkul
# Create your views here.

def productbuat(request):
	if request.method == "POST":
		form = matkulform(request.POST)
		if form.is_valid():
			simpan = form.save()
			simpan.save()
	else:
		form = matkulform()

	matkulkeluar = matkul.objects.all()
	context = {
		'form' : form,
		'matkulkeluar':matkulkeluar,
	}
	return render(request,'nyimpen.html',context)

def detail(request,qid):
 	context ={'matkul': matkul.objects.get(id=qid)}    
 	return render (request,'detail.html',context)

def hapus(request,qid):
 	matkul.objects.get(id=qid).delete()
 	return redirect ('story5:productbuat')