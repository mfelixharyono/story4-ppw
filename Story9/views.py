from django.shortcuts import render,redirect
from django.contrib.auth.forms import UserCreationForm
from .forms import CreateUserForm
# Create your views here.
def signup(request):
	if request.method == 'POST':
		form = CreateUserForm(request.POST)
		if form.is_valid():
			user = form.save()
			return redirect('Story9:home')
			
	else:
		form = CreateUserForm()
		
	return render(request,'signup.html', {
		'form': form
	});

def home(request):
	return render(request,'home.html')