from django.contrib.auth.forms import UserCreationForm
from django import forms
from django.contrib.auth.models import User
from django.forms import ModelForm

class CreateUserForm(UserCreationForm):
	"""docstring for CreateUserForm"""
	class Meta:
		model = User
		fields = {'username','password1','password2'}
		
