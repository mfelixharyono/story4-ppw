from django.urls import path

from . import views

app_name = 'Story9'

urlpatterns = [
	path('home/',views.home,name='home'),
	path('', views.signup, name='signup'), 
]
