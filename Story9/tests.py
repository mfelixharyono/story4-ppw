from django.test import TestCase, Client
from django.urls import resolve
from .views import home,signup
from .forms import CreateUserForm

class story9Test(TestCase):

    def test_Story9_home_ada(self):
        response = Client().get('/Story9/home')
        self.assertEqual(response.status_code,301)

    def test_signup_ada(self):
    	response = Client().get('/Story9/')
    	self.assertEqual(response.status_code,200)

    def test_login_ada(self):
    	response = Client().get('/login/')
    	self.assertEqual(response.status_code,200)

    def test_logout_ada(self):
    	response=Client().get('/logout/')
    	self.assertEqual(response.status_code,200)