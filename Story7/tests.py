from django.test import TestCase, Client
from django.urls import resolve
from .views import accord

class story6Test(TestCase):

    def test_Story7_accord_ada(self):
        response = Client().get('/Story7/')
        self.assertEqual(response.status_code,200)

    def test_main_daftarkegiatan_func(self):
        found = resolve('/Story7/')
        self.assertEqual(found.func,accord)

    def test_html(self):
    	response = Client().get('/Story7/')
    	html_kembalian = response.content.decode('utf8')
    	self.assertIn('Michael Felix', html_kembalian)
    	self.assertTemplateUsed(response, 'accord.html')